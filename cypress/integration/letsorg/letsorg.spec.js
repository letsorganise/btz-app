/// <reference types="Cypress" />

context('Test', () => {
  beforeEach(() => {
    cy.visit('https://localhost');
  });
  it('login', () => {
    cy.get(
      ':nth-child(1) > .col-sm-8 > .q-field > :nth-child(1) > .q-field-content > .q-if > .q-if-inner > .col'
    )
      .click()
      .type('@organiser')
      .should('have.value', '@organiser');
    cy.get(
      ':nth-child(3) > .col-sm-8 > .q-field > :nth-child(1) > .q-field-content > .q-if > .q-if-inner > .col'
    )
      .click()
      .type('password1')
      .should('have.value', 'password1');
    cy.get('.col-xs-12 > .q-btn').click();
  });
});
