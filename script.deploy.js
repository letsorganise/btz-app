#!/usr/bin/env node

const argv = require('yargs').argv;
require('dotenv').config();
require('colors');

const shell = require('shelljs');

const git = shell.exec('git symbolic-ref -q HEAD', { silent: true }).stdout.split('/');
const branch = git[git.length - 1];
let command = 'rsync -azv --force --delete -e "ssh -p22" ./dist/spa-mat/* ';
let server = '';

if (argv.live) {
  if (branch !== 'master\n') {
    console.log('Can only deploy live from'.red, `${branch} Branch`.bold.red);
    process.exit(1);
  }
  server = process.env.SERVER_LIVE;
  shell.exec('LIVE=true npm run build');
  exeSync();
} else if (argv.beta) {
  shell.exec('npm run build');
  server = process.env.SERVER_BETA;
  console.log('Deploying Beta'.red, `Branch: ${branch}`.bold.red);
  exeSync();
} else {
  console.error('Must provide argument beta or live'.bold.red);
  process.exit(1);
}

function exeSync() {
  command += server;
  console.log('Deploying with rsync'.bold.green);
  shell.exec(command, { async: true, silent: true }, function(code, stdout, stderr) {
    console.log(stdout.toString().cyan);
    if (code === 0) {
      console.log('Deployed to: '.green, server.bold.green);
    } else {
      console.log(stderr.toString().red);
    }
  });
}
