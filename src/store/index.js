import Vue from 'vue';
import Vuex from 'vuex';

import auth from './auth';
import chats from './chats';
import quotes from './quotes';
import sites from './sites';
import users from './users';
import workers from './workers';
import unions from './unions';
import groups from './groups';
import settings from './settings';
import meetings from './meetings';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    chats,
    quotes,
    sites,
    users,
    unions,
    workers,
    groups,
    settings,
    meetings
  }
});

export default store;
