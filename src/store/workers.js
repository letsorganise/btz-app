import { isDataOld, notifyErrors, http } from '../utils';

const SET_WORKERS = 'SET_WORKERS';
const SET_RELATION_TYPES = 'SET_RELATION_TYPES';
const SET_ATTRIBUTES = 'SET_ATTRIBUTES';
const ADD_OR_UPDATE_WORKER = 'ADD_OR_UPDATE_WORKER';

const state = {
  workers: [],
  lastFetched: null,
  relationTypes: [],
  attributes: []
};

const mutations = {
  [SET_WORKERS](state, data) {
    state.lastFetched = new Date();
    state.workers = data;
  },
  [ADD_OR_UPDATE_WORKER](state, data) {
    const index = state.workers.findIndex(worker => worker.id === data.id);
    if (index > -1) {
      state.workers.splice(index, 1, data);
    } else {
      state.workers.push(data);
    }
  },
  [SET_RELATION_TYPES](state, types) {
    state.relationTypes = types;
  },
  [SET_ATTRIBUTES](state, attribs) {
    state.attributes = attribs;
  }
};

const actions = {
  addNewRelation: ({ commit }, { id, data }) => {
    return http({
      url: '/workers/' + id + '/relations',
      method: 'post',
      data
    })
      .then(({ data }) => {
        // console.error(data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  fetchRelationTypes: ({ commit }) => {
    http({
      url: '/relation-types',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_RELATION_TYPES, data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  editWorker: ({ commit, getters }, data) => {
    return http({
      url: '/workers/' + data.id,
      method: 'put',
      data
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_WORKER, data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  fetchWorkers: ({ state, commit }) => {
    if (isDataOld(state.lastFetche)) {
      http({
        url: '/workers?include=sites:name:id&include=unions',
        method: 'get'
      })
        .then(({ data }) => {
          commit(SET_WORKERS, data);
        })
        .catch(error => {
          notifyErrors(error);
          // console.error(error);
        });
    }
  },
  fetchWorkerById: ({ commit }, id) => {
    return http({
      url: `/workers/${id}?include=sites:name:id`,
      method: 'get'
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_WORKER, data);
      })
      .catch(error => {
        console.error(error);
      });
  },
  fetchAttributes: ({ state, commit }) => {
    http({
      url: '/attribute-types',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_ATTRIBUTES, data);
      })
      .catch(error => {
        console.error(error);
      });
  }
};

const getters = {
  workers: state => state.workers,

  workerById: state => id => state.workers.find(worker => worker.id === id),

  relationTypes: state => state.relationTypes,

  attributes: state => state.attributes
};

export default {
  state,
  mutations,
  actions,
  getters
};
