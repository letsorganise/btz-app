import { notifyErrors, http } from '../utils';

const SET_USERS = 'SET_USERS';
const ADD_OR_UPDATE_USER = 'ADD_OR_UPDATE_USER';
const ADD_OR_UPDATE_INVITATION = 'ADD_OR_UPDATE_INVITATION';
const SET_INVITATIONS = 'SET_INVITATIONS';

const state = {
  currentId: null,
  invitations: [],
  users: []
};

const mutations = {
  [SET_USERS](state, data) {
    state.users = data;
  },

  [SET_INVITATIONS](state, data) {
    state.invitations = data;
  },

  [ADD_OR_UPDATE_USER](state, data) {
    const index = state.users.findIndex(entry => entry.id === data.id);
    if (index > -1) {
      state.users.splice(index, 1, data);
    } else {
      state.users.push(data);
    }
    // state.users = users;
  },
  [ADD_OR_UPDATE_INVITATION](state, data) {
    const index = state.invitations.findIndex(entry => entry.id === data.id);
    if (index > -1) {
      state.invitations.splice(index, 1, data);
    } else {
      state.invitations.push(data);
    }
    // state.users = users;
  }
};

const actions = {
  sendUserInvitation: ({ commit, state }, data) => {
    return http({
      url: '/user-invitations',
      method: 'post',
      data
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_INVITATION, data);
        return Promise.resolve(data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  updateUserInvitation: ({ commit, state }, data) => {
    return http({
      url: '/user-invitations/' + data.id,
      method: 'put',
      data
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_INVITATION, data);
        return Promise.resolve(data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  deleteUserInvitation: ({ commit, state }, id) => {
    return http({
      url: '/user-invitations/' + id,
      method: 'delete'
    })
      .then(() => {
        commit(SET_INVITATIONS, state.invitations.filter(m => m.id !== id));
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
        return Promise.reject(error);
      });
  },
  updateUser: ({ commit, state }, user) => {
    return http({
      url: '/users/' + user.id,
      method: 'put',
      data: user
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_USER, data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
        return Promise.reject(error);
      });
  },
  deleteUser: ({ commit, state }, user) => {
    return http({
      url: '/users/' + user.id,
      method: 'delete'
    })
      .then(({ data }) => {
        commit(SET_USERS, state.users.filter(u => u.id !== user.id));
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchUsers: ({ state, commit }) => {
    return http({
      url: '/users',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_USERS, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchUserInvitations: ({ state, commit }) => {
    return http({
      url: '/user-invitations',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_INVITATIONS, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchUserById: ({ state, commit }, id) => {
    return http({
      url: `/users/${id}?include=worker&include=groups&include=settings`,
      method: 'get'
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_USER, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  }
};

const getters = {
  users: state => state.users,
  userInvitations: state => state.invitations,
  userById: state => id => state.users.find(user => user.id === id)
};

export default {
  state,
  mutations,
  actions,
  getters
};
