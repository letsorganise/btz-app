import { LocalStorage } from 'quasar';
const SET_SOUND = 'SET_SOUND';
const SET_ONLINE = 'SET_ONLINE';

const state = {
  playSound: LocalStorage.get.item(SET_SOUND),
  isOnline: false
};

const mutations = {
  [SET_SOUND](state, data) {
    state.playSound = data;
  },
  [SET_ONLINE](state, data) {
    state.isOnline = data;
  }
};

const actions = {
  setSound: ({ commit, state }, value) => {
    commit(SET_SOUND, value);
    LocalStorage.set(SET_SOUND, value);
  },
  setOnline: ({ commit, state }, value) => {
    commit(SET_ONLINE, value);
  }
};

const getters = {
  playSound: state => state.playSound,
  isOnline: state => state.isOnline
};

export default {
  state,
  mutations,
  actions,
  getters
};
