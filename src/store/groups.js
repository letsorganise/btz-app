import { notifyErrors, http } from '../utils';

const SET_GROUPS = 'SET_GROUPS';
const SET_GROUP_TYPES = 'SET_GROUP_TYPES';
const ADD_OR_UPDATE_GROUP = 'ADD_OR_UPDATE_GROUP';

const state = {
  groups: [],
  groupTypes: []
};

const mutations = {
  [SET_GROUPS](state, data) {
    state.groups = data;
  },
  [ADD_OR_UPDATE_GROUP](state, data) {
    const index = state.groups.findIndex(group => group.id === data.id);
    if (index > -1) {
      state.groups.splice(index, 1, Object.assign(state.groups[index], data));
    } else {
      state.groups.push(data);
    }
  },
  [SET_GROUP_TYPES](state, types) {
    state.groupTypes = types;
  }
};

const actions = {
  createGroup: ({ commit, state }, group) => {
    http({
      url: '/groups',
      method: 'post',
      data: group
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_GROUP, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchGroups: ({ state, commit }) => {
    http({
      url: '/groups',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_GROUPS, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchGroupById: ({ state, commit }, id) => {
    http({
      url: `/groups/${id}`,
      method: 'get'
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_GROUP, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchGroupTypes: ({ state, commit }, id) => {
    http({
      url: `/group-types`,
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_GROUP_TYPES, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  }
};

const getters = {
  groups: state => state.groups,
  groupById: state => id => state.groups.find(group => group.id === id),
  groupTypes: state => state.groupTypes
};

export default {
  state,
  mutations,
  actions,
  getters
};
