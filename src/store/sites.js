import { isDataOld, notifyErrors, http } from '../utils';

const SET_SITES = 'SET_SITES';
const ADD_OR_UPDATE_SITE = 'ADD_OR_UPDATE_SITE';
const SET_SIITES_LOADING = 'SET_SIITES_LOADING';

const state = {
  lastFetched: null,
  sites: [],
  loading: false
};

const mutations = {
  [SET_SITES](state, data) {
    state.sites = data;
    state.lastFetched = new Date();
  },

  [ADD_OR_UPDATE_SITE](state, data) {
    const index = state.sites.findIndex(site => site.id === data.id);
    if (index > -1) {
      state.sites.splice(index, 1, data);
    } else {
      state.sites.push(data);
    }
  },
  [SET_SIITES_LOADING](state, data) {
    state.loading = !!data;
  }
};

const actions = {
  createSite: ({ commit, state }, user) => {
    http({
      url: '/sites',
      method: 'post',
      data: user
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_SITE, data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  addNewWorker: ({ commit, getters }, { id, data }) => {
    return http({
      url: '/sites/' + id + '/workers',
      method: 'post',
      data
    })
      .then(({ data }) => {
        const site = getters.siteById(id);
        if (Array.isArray(site.Workers)) {
          site.Workers.push(data);
        }
        commit(ADD_OR_UPDATE_SITE, site);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },

  fetchSites: ({ state, commit }) => {
    if (isDataOld(state.lastFetche) && !state.loading) {
      commit(SET_SIITES_LOADING, true);
      http({
        url: '/sites',
        method: 'get'
      })
        .then(({ data }) => {
          commit(SET_SITES, data);
          commit(SET_SIITES_LOADING, false);
        })
        .catch(error => {
          // console.error(error);
          notifyErrors(error);
          commit(SET_SIITES_LOADING, false);
        });
    }
  },
  fetchSiteById: ({ getters, commit }, id) => {
    return http({
      url: `/sites/${id}?include=workers`,
      method: 'get'
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_SITE, data);
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  }
};

const getters = {
  sites: state => state.sites,
  siteById: state => id => state.sites.find(site => site.id === id)
};

export default {
  state,
  mutations,
  actions,
  getters
};
