import { notifyErrors, http, trackEvent } from '../utils';
import { LocalStorage } from 'quasar';

const SET_CURRENT_USER = 'SET_CURRENT_USER';
const LOG_IN = 'LOG_IN';
const LOG_OUT = 'LOG_OUT';

const state = {
  currentUser: {},
  loggedIn: false
};

const mutations = {
  [SET_CURRENT_USER](state, data) {
    state.currentUser = data;
  },

  [LOG_OUT](state) {
    state.errors = [];
    state.loggedIn = false;
    state.currentUser = {};
    state.loading = false;
    LocalStorage.remove('session_date');
  },

  [LOG_IN](state) {
    state.loggedIn = true;
    state.loading = false;
  }
};

const actions = {
  authenticate: ({ commit, state }, creds) => {
    return http({
      url: '/auth',
      method: 'post',
      data: creds
    })
      .then(({ data }) => {
        commit(LOG_IN);
        commit(SET_CURRENT_USER, data.user);
        LocalStorage.set('session_date', new Date());
        return data;
      })
      .catch(error => {
        trackEvent('Login', 'Failed', creds.id);
        notifyErrors(error);
        commit(LOG_OUT);
      });
  },
  logout: ({ commit }) => {
    commit(LOG_OUT);
    return http({
      url: '/auth',
      method: 'delete'
    }).catch(error => {
      notifyErrors(error);
      // console.error(error);
    });
  },

  checkAuth: ({ state, commit }) => {
    return http({
      url: '/auth',
      method: 'get'
    })
      .then(({ data }) => {
        commit(LOG_IN);
        commit(SET_CURRENT_USER, data.user);
        return data.user;
      })
      .catch(() => {
        commit(LOG_OUT);
      });
  }
};
const isRoot = state => (state.currentUser.id === '@root' ? 8 : 0);
const isAdmin = state => {
  let isAdmin = isRoot(state);

  state.currentUser.Groups &&
    state.currentUser.Groups.map(g => {
      if (g.type === 'ADMIN') {
        isAdmin = g.permission > isAdmin ? g.permission : isAdmin;
      }
    });
  return isAdmin;
};

const isOrganiser = state => {
  let isOrganiser = isAdmin(state);

  state.currentUser.Groups &&
    state.currentUser.Groups.map(g => {
      if (g.type === 'ORGANISER') {
        isOrganiser = g.permission > isOrganiser ? g.permission : isOrganiser;
      }
    });
  return isOrganiser;
};

const isActivist = state => {
  let isActivist = isOrganiser(state);

  state.currentUser.Groups &&
    state.currentUser.Groups.map(g => {
      if (g.type === 'ACTIVIST') {
        isActivist = g.permission > isActivist ? g.permission : isActivist;
      }
    });
  return isActivist;
};

const getters = {
  loggedIn: state => state.loggedIn,
  me: state => state.currentUser,
  myGroups: state => state.currentUser.Groups && state.currentUser.Groups,
  isAdmin,
  isRoot,
  isOrganiser,
  isActivist
};

export default {
  state,
  mutations,
  actions,
  getters
};
