import { isDataOld, notifyErrors, http } from '../utils';

const UPDATE_QUOTE_TIMESTAMP = 'UPDATE_QUOTE_TIMESTAMP';
const SET_QUOTES = 'SET_QUOTES';
const ADD_QUOTE = 'ADD_QUOTE';

const state = {
  lastFetched: null,
  quotes: []
};

const mutations = {
  [UPDATE_QUOTE_TIMESTAMP](state) {
    state.lastFetched = new Date();
  },

  [SET_QUOTES](state, data) {
    state.quotes = data;
  },

  [ADD_QUOTE](state, data) {
    state.quotes.push(data);
  }
};

const actions = {
  createNewQuote: ({ commit }, data) => {
    return http({
      url: '/quotes',
      method: 'post',
      data
    })
      .then(({ data }) => {
        commit(ADD_QUOTE, data);
        commit(UPDATE_QUOTE_TIMESTAMP);
        return Promise.resolve(data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchQuotes: ({ commit }) => {
    if (isDataOld(state.lastFetched)) {
      http({
        url: '/quotes',
        method: 'get'
      })
        .then(({ data }) => {
          commit(SET_QUOTES, data);
          commit(UPDATE_QUOTE_TIMESTAMP);
          return Promise.resolve(data);
        })
        .catch(error => {
          // console.error(error);
          notifyErrors(error);
        });
    }
  },
  deleteQuote: ({ commit, state }, id) => {
    return http({
      url: '/quotes/' + id,
      method: 'delete'
    })
      .then(() => {
        commit(SET_QUOTES, state.quotes.filter(q => q.id !== id));
        return Promise.resolve();
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  }
};

const getters = {
  quotes: state => state.quotes
};

export default {
  state,
  mutations,
  actions,
  getters
};
