import { notifyErrors, http } from '../utils';

const SET_UNIONS = 'SET_UNIONS';
const SET_UNION_NAMES = 'SET_UNION_NAMES';
const ADD_OR_UPDATE_UNION = 'ADD_OR_UPDATE_UNION';

const state = {
  unions: [],
  unionNames: []
};

const mutations = {
  [SET_UNIONS](state, data) {
    state.unions = data;
  },
  [SET_UNION_NAMES](state, data) {
    state.unionNames = data;
  },
  [ADD_OR_UPDATE_UNION](state, data) {
    const index = state.unions.findIndex(union => union.id === data.id);
    if (index > -1) {
      state.unions.splice(index, 1, data);
    } else {
      state.unions.push(data);
    }
  }
};

const actions = {
  createUnion: ({ commit, state }, data) => {
    http({
      url: '/unions',
      method: 'post',
      data
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_UNION, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  createUnionBranch: ({ commit, state }, data) => {
    http({
      url: '/unions/branch',
      method: 'post',
      data
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_UNION, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchUnions: ({ state, commit }) => {
    http({
      url: '/unions',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_UNIONS, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchUnionNames: ({ state, commit }) => {
    http({
      url: '/union-names',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_UNION_NAMES, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  }
};

const getters = {
  unions: state => state.unions,
  unionNames: state => state.unionNames
};

export default {
  state,
  mutations,
  actions,
  getters
};
