import { notifyErrors, http } from '../utils';

const SET_CHATS = 'SET_CHATS';
const SET_CHAT_USERS = 'SET_CHAT_USERS';
const ADD_CHAT = 'ADD_CHAT';
const SET_ONLINE_USERS = 'SET_ONLINE_USERS';
const ADD_OR_UPDATE_CHAT = 'ADD_OR_UPDATE_CHAT';
const ADD_OR_UPDATE_CHAT_USER = 'ADD_OR_UPDATE_CHAT_USER';
const ADD_MESSAGE = 'ADD_MESSAGE';
const DELETE_MESSAGE = 'DELETE_MESSAGE';
const ADD_FILE = 'ADD_FILE';
const DELETE_FILE = 'DELETE_FILE';
const UPDATE_MESSAGE = 'UPDATE_MESSAGE';

const state = {
  lastFetched: null,
  onlineUsers: [],
  chats: [],
  chatUsers: []
};

const mutations = {
  [SET_CHATS](state, data) {
    state.chats = data;
  },

  [SET_CHAT_USERS](state, data) {
    state.chatUsers = data;
  },

  [ADD_CHAT](state, data) {
    state.chats.push(data);
  },

  [ADD_OR_UPDATE_CHAT](state, data) {
    const index = state.chats.findIndex(chat => chat.id === data.id);
    if (index > -1) {
      state.chats.splice(index, 1, data);
    } else {
      state.chats.push(data);
    }
  },

  [ADD_OR_UPDATE_CHAT_USER](state, data) {
    const index = state.chatUsers.findIndex(cu => cu.id === data.id);
    if (index > -1) {
      state.chatUsers.splice(index, 1, data);
    } else {
      state.chatUsers.push(data);
    }
  },

  [ADD_MESSAGE](state, data) {
    const chat = state.chats.find(c => c.id === data.ChatId);
    if (chat) {
      chat.Messages.push(data);
      chat.lastActive = data.lastActive;
    }
  },

  [ADD_FILE](state, data) {
    const chat = state.chats.find(c => c.id === data.ChatId);
    if (chat) {
      chat.Filse.push(data);
      chat.lastActive = data.lastActive;
    }
  },

  [DELETE_FILE](state, data) {
    const chat = state.chats.find(c => c.id === data.ChatId);
    if (chat) {
      chat.Files = chat.Files.filter(m => m.id !== data.id);
      chat.lastActive = Date();
    }
  },

  [UPDATE_MESSAGE](state, data) {
    const chat = state.chats.find(c => c.id === data.ChatId);
    if (chat) {
      const index = chat.Messages.findIndex(m => m.id === data.id);
      const message = chat.Messages.find(m => m.id === data.id);
      if (index > -1) {
        chat.Messages.splice(index, 1, { ...message, ...data });
        chat.lastActive = data.lastActive;
      }
    }
  },

  [DELETE_MESSAGE](state, data) {
    const chat = state.chats.find(c => c.id === data.ChatId);
    if (chat) {
      chat.Messages = chat.Messages.filter(m => m.id !== data.id);
      chat.lastActive = Date();
    }
  },

  [SET_ONLINE_USERS](state, data) {
    state.onlineUsers = data;
  }
};

const actions = {
  addMessageToChat: ({ commit }, payload) => {
    commit(ADD_MESSAGE, payload);
  },
  setOnlineUsers: ({ commit }, payload) => {
    commit(SET_ONLINE_USERS, payload);
  },

  createNewChat: ({ commit }, data) => {
    return http({
      url: '/chats',
      method: 'post',
      data
    })
      .then(({ data }) => {
        commit(ADD_CHAT, data);
        return data;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  updateChat: ({ commit }, data) => {
    if (!data.id) return;
    return http({
      url: `/chats/${data.id}`,
      method: 'put',
      data
    })
      .then(({ data }) => {
        // commit(ADD_OR_UPDATE_CHAT, data);
        return data;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  updateChatUser: ({ commit }, data) => {
    return http({
      url: `/chat-users/${data.id}`,
      method: 'put',
      data
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_CHAT_USER, data);
        return data;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  fetchChats: ({ commit }) => {
    http({
      url: '/chats',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_CHATS, data);
        return data;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });

    return http({
      url: '/chat-users',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_CHAT_USERS, data);
        return data;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  fetchChatById: ({ commit }, id) => {
    if (!id) return;
    return http({
      url: `/chats/${id}`,
      method: 'get'
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_CHAT, data);
        return http({
          url: `/chats/${id}/user`,
          method: 'get'
        })
          .then(({ data }) => {
            commit(ADD_OR_UPDATE_CHAT_USER, data);
            return data;
          })
          .catch(error => {
            notifyErrors(error);
            // console.error(error);
            return Promise.reject(error);
          });
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  leaveChat: ({ commit, state }, id) => {
    if (!id) return;
    return http({
      url: `/chats/${id}/user`,
      method: 'delete'
    })
      .then(() => {
        commit(SET_CHATS, state.chats.filter(c => c.id !== id));
        commit(SET_CHAT_USERS, state.chatUsers.filter(cu => cu.ChatId !== id));
        return true;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  deleteMessage: ({ commit, state }, message) => {
    if (!message.id) return;
    return http({
      url: `/messages/${message.id}`,
      method: 'delete'
    })
      .then(() => {
        commit(DELETE_MESSAGE, message);
        return true;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  },
  deleteChatFile: ({ commit, state }, file) => {
    if (!file.id) return;
    return http({
      url: `/chats/${file.ChatId}/files/${file.id}`,
      method: 'delete'
    })
      .then(() => {
        commit(DELETE_FILE, file);
        return true;
      })
      .catch(error => {
        notifyErrors(error);
        // console.error(error);
      });
  }
};

const getters = {
  chats: state => state.chats,
  chatUserByChat: state => id => state.chatUsers.find(cu => cu.ChatId === id),
  chatById: state => id => state.chats.find(chat => chat.id === id),
  onlineUsers: state => state.onlineUsers,
  offlineUsers: (state, getters) => {
    // console.log(getters.users, state.onlineUsers);
    if (getters.users.length && state.onlineUsers.length) {
      const online = state.onlineUsers.map(u => u.id);
      return getters.users.filter(u => !online.includes(u.id));
    }
    return [];
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
