import { notifyErrors, http } from '../utils';

const SET_MEETINGS = 'SET_MEETINGS';
const ADD_OR_UPDATE_MEETING = 'ADD_OR_UPDATE_MEETING';

const state = {
  meetings: [
    {
      id: 'e3957d7d-fbda-47d1-9ac9-face82e020d4',
      location: 'Board Room',
      start: 'Sun Jun 03 2018 12:05:58 GMT+0100 (BST)',
      finish: 'Sun Jun 03 2018 09:05:58 GMT+0100 (BST)',
      createdBy: '@root',
      Participants: [{ id: '@root', name: 'Root' }, { id: '@orgniser', name: 'Organiser' }],
      notes: `Remaining essentially unchanged. It was Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
    },
    {
      id: 'd604849e-e662-47bf-965b-3233078e4dd1',
      location: 'City Center',
      start: 'Sun Jun 02 2018 12:05:58 GMT+0100 (BST)',
      finish: null,
      createdBy: '@orgniser',
      Participants: [{ id: '@root', name: 'Root' }, { id: '@orgniser', name: 'Organiser' }],
      notes: `Lorem Ipsum is simply dummy text of the printing and typLorem Ipsum.`
    }
  ]
};

const mutations = {
  [SET_MEETINGS](state, data) {
    state.meetings = data;
  },
  [ADD_OR_UPDATE_MEETING](state, data) {
    const index = state.meetings.findIndex(meeting => meeting.id === data.id);
    if (index > -1) {
      state.meetings.splice(index, 1, Object.assign(state.meetings[index], data));
    } else {
      state.meetings.push(data);
    }
  }
};

const actions = {
  createMeeting: ({ commit, state }, meeting) => {
    meeting.id = Math.random().toString();
    commit(ADD_OR_UPDATE_MEETING, meeting);
    return Promise.resolve(meeting);
    // http({
    //   url: '/meetings',
    //   method: 'post',
    //   data: meeting
    // })
    //   .then(({ data }) => {
    //     commit(ADD_OR_UPDATE_MEETING, data);
    //   })
    //   .catch(error => {
    //     // console.error(error);
    //     notifyErrors(error);
    //   });
  },
  fetchMeetings: ({ state, commit }) => {
    http({
      url: '/meetings',
      method: 'get'
    })
      .then(({ data }) => {
        commit(SET_MEETINGS, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  },
  fetchMeetingById: ({ state, commit }, id) => {
    http({
      url: `/meetings/${id}`,
      method: 'get'
    })
      .then(({ data }) => {
        commit(ADD_OR_UPDATE_MEETING, data);
      })
      .catch(error => {
        // console.error(error);
        notifyErrors(error);
      });
  }
};

const getters = {
  meetings: state => state.meetings,
  meetingById: state => id => state.meetings.find(meeting => meeting.id === id)
};

export default {
  state,
  mutations,
  actions,
  getters
};
