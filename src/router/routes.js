import offlineLayout from 'layouts/offline-layout';

import store from '../store';

let entryUrl = null;

const guard = async (to, from, next) => {
  if (store.state.auth.loggedIn) {
    if (entryUrl) {
      const url = entryUrl;
      entryUrl = null;
      return next(url);
    } else {
      return next();
    }
  }

  await store.dispatch('checkAuth');
  if (store.state.auth.loggedIn) {
    next();
  } else {
    // console.error(to.path);
    entryUrl = to.path;
    next('/login');
  }
};

export default [
  {
    path: '/',
    beforeEnter: guard,
    redirect: '/chats'
  }, // Default
  {
    path: '/admin',

    beforeEnter: guard,
    component: () => import('layouts/admin-layout'),
    children: [
      { path: '', name: 'Admin', component: () => import('pages/users-id') },
      { path: 'users', name: 'Users', component: () => import('pages/users') },
      { path: 'users/:id', name: 'User:id', component: () => import('pages/users-id') },
      { path: 'groups', name: 'Groups', component: () => import('pages/groups') },
      { path: 'groups/:id', name: 'Groups:id', component: () => import('pages/groups-id') },
      { path: 'unions', name: 'Unions', component: () => import('pages/unions') },
      {
        path: 'invitations',
        name: 'Invitations',
        component: () => import('pages/user-invitations')
      }
    ]
  },
  {
    path: '/quotes',
    beforeEnter: guard,
    component: () => import('layouts/quotes-layout'),
    children: [{ path: '', name: 'Quotes', component: () => import('pages/quotes') }]
  },
  {
    path: '/chats',
    beforeEnter: guard,
    component: () => import('layouts/chat-layout'),
    children: [
      { path: '', name: 'Chats', component: () => import('pages/chats') },
      { path: ':id', name: 'Chats:id', component: () => import('pages/chats-id') }
    ]
  },
  {
    path: '/meetings',
    beforeEnter: guard,
    component: () => import('layouts/meetings-layout'),
    children: [
      { path: '', name: 'Meetings', component: () => import('pages/meetings') },
      { path: 'new', name: 'Meetings New', component: () => import('pages/meetings-new') }
    ]
  },
  {
    path: '/help',
    beforeEnter: guard,
    component: () => import('layouts/help-layout'),
    children: [{ path: '', name: 'Help', component: () => import('pages/faq') }]
  },

  // {
  //   path: '/membership',
  //   beforeEnter: guard,
  //   component: ()=>import('layouts/membership-layout'),
  //   children: [
  //     { path: '', component: ()=>import('pages/membership') },
  //     { path: 'sites', component: ()=>import('pages/sites') },
  //     { path: 'sites/:id', component: ()=>import('pages/sites-id') },
  //     { path: 'sites/:id/graph', component: ()=>import('pages/site-id-graph') }
  //   ]
  // },

  {
    path: '/login',
    component: offlineLayout,
    children: [{ path: '', name: 'Login', component: () => import('pages/login') }]
  },

  {
    path: '/confirm',
    component: offlineLayout,
    children: [
      {
        path: 'reset-password/:token',
        name: 'Reset Password:token',
        component: () => import('pages/reset-password')
      },
      {
        path: 'join/:token',
        name: 'Join:token',
        component: () => import('pages/join')
      }
    ]
  },
  {
    path: '/not-enabled',
    component: offlineLayout,
    children: [{ path: '', name: 'Not Enabled', component: () => import('pages/not-enabled') }]
  },
  {
    // Always leave this as last one
    path: '*',
    name: 'Not Found 404',
    component: () => import('pages/notFound404')
  }
];
