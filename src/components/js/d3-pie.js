import d3 from './d3import';

export const init = (element, labels = {}, width = 960, donut = false) => {
  const chart = {};
  // let svg, g, label, path, color, pie;
  chart.color = d3.scaleOrdinal(d3.schemeAccent);

  chart.pie = d3
    .pie()
    .sort(null)
    .value(d => d.percent);

  d3
    .select(element)
    .select('svg')
    .remove();

  chart.width = width;
  chart.height = width * 0.65;
  chart.svg = d3
    .select(element)
    .append('svg')
    .attr('position', 'relative')
    .attr('width', width)
    .attr('height', chart.height);

  const radius = Math.min(width, chart.height) / 2.1;
  const innerRadius = radius - radius * 0.3;
  chart.label = d3
    .arc()
    .outerRadius(radius)
    .innerRadius(innerRadius);
  chart.path = d3
    .arc()
    .outerRadius(radius)
    .innerRadius(donut ? innerRadius : 0)
    .cornerRadius(radius / 20);

  chart.g = chart.svg
    .append('g')
    .attr('transform', 'translate(' + width / 1.65 + ',' + chart.height / 2 + ')');
  if (labels.label) {
    chart.svg
      .append('text')
      .attr('transform', 'translate(20,30)')
      .attr('font-size', '2em')
      .text(labels.label);
  }
  if (labels.sublable) {
    chart.svg
      .append('text')
      .attr('transform', 'translate(20,60)')
      .attr('font-size', chart.width * 0.022)
      .text(labels.sublable);
  }
  return chart;
};

export const render = (chart, dataset) => {
  const totalValue = dataset.reduce(function(prev, curr) {
    return prev + curr.value;
  }, 0);

  dataset = dataset.filter(d => d.value).map(d => {
    const res = d;
    res.percent = d.value / totalValue * 100;
    return res;
  });
  const arc = chart.g
    .selectAll('.arc')
    .data(chart.pie(dataset))
    .enter()
    .append('g')
    .attr('class', 'arc');

  arc
    .append('path')
    .attr('d', chart.path)
    .attr('fill', d => chart.color(d.data.label));

  // const textLabels =
  chart.g
    .selectAll('label')
    .data(chart.pie(dataset))
    .enter()
    .append('text')
    .attr('class', 'label')
    .attr('font-size', chart.width * 0.022)
    .attr('transform', d => 'translate(' + chart.label.centroid(d) + ')')
    .attr('dy', '0.35em')
    .text(d => `${Number(d.data.percent).toFixed(2)}%`);

  // Setup legend

  const legendDotSize = chart.width * 0.048; // 35;
  const legendWrapper = chart.svg
    .append('g')
    .attr('class', 'legend')
    .attr('transform', d => 'translate(' + chart.width * 0.02 + ',' + chart.height * 0.1 + ')');

  // The text of the legend
  const legendText = legendWrapper.selectAll('text').data(dataset);

  legendText
    .enter()
    .append('text')
    .attr('y', (d, i) => i * legendDotSize + legendDotSize / 2.8)
    .attr('x', legendDotSize)
    .merge(legendText)
    .attr('font-size', legendDotSize / 2)
    .text(d => `(${d.value}) ${d.label}`);

  legendText.exit().remove();

  // The dots of the legend
  const legendDot = legendWrapper.selectAll('rect').data(dataset);

  legendDot
    .enter()
    .append('rect')
    .attr('y', (d, i) => i * legendDotSize)
    .attr('rx', legendDotSize * 0.5)
    .attr('ry', legendDotSize * 0.5)
    .attr('width', legendDotSize * 0.5)
    .attr('height', legendDotSize * 0.5)
    .merge(legendDot)
    .style('fill', d => chart.color(d.label));

  legendDot.exit().remove();

  // const alpha = 0.5;
  // const spacing = 12;
  // function relax() {
  //   let again = false;
  //   textLabels.each(function(d, i) {
  //     const a = this;
  //     const da = d3.select(a);
  //     const y1 = da.attr('y');
  //     textLabels.each(function(d, j) {
  //       const b = this;
  //       // a & b are the same element and don't collide.
  //       if (a === b) return;
  //       const db = d3.select(b);
  //       // a & b are on opposite sides of the chart and
  //       // don't collide
  //       if (da.attr('text-anchor') !== db.attr('text-anchor')) return;
  //       // Now let's calculate the distance between
  //       // these elements.
  //       const y2 = db.attr('y');
  //       const deltaY = y1 - y2;

  //       // If spacing is greater than our specified spacing,
  //       // they don't collide.
  //       if (Math.abs(deltaY) > spacing) return;

  //       // If the labels collide, we'll push each
  //       // of the two labels up and down a little bit.
  //       again = true;
  //       const sign = deltaY > 0 ? 1 : -1;
  //       const adjust = sign * alpha;
  //       da.attr('y', +y1 + adjust);
  //       db.attr('y', +y2 - adjust);
  //     });
  //   });
  //   // Adjust our line leaders here
  //   // so that they follow the labels.
  //   if (again) {
  //     setTimeout(relax, 20);
  //   }
  // }

  // relax();
};
