import * as forceSimulation from 'd3-force';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Selection from 'd3-selection';
import * as d3Axis from 'd3-axis';
import * as d3ScaleChromatic from 'd3-scale-chromatic';

const d3 = Object.assign(
  {},
  d3Axis,
  d3Scale,
  d3ScaleChromatic,
  d3Selection,
  d3Shape,
  forceSimulation
);

export default d3;
