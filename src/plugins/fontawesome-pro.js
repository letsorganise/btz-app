import '@fortawesome/fontawesome-pro-webfonts/css/fontawesome.css';
import '@fortawesome/fontawesome-pro-webfonts/css/fa-solid.css';
import '@fortawesome/fontawesome-pro-webfonts/css/fa-regular.css';
// do you want these too?
import '@fortawesome/fontawesome-pro-webfonts/css/fa-light.css';
// import '@fortawesome/fontawesome-pro-webfonts/css/fa-brands.css'

export default () => {
  // Leave blank or make something cool.
};
