// import something here
// import { Platform } from 'quasar';

// const DOMAIN = process.env.DOMAIN;

export default ({ app, router, Vue }) => {
  // if (Platform.is.cordova) {
  //   const script = document.createElement('script');
  //   script.type = 'text/javascript';
  //   script.setAttribute('src', DOMAIN + '/api/socket.io/socket.io.js');

  //   document.body.appendChild(script);

  //   script.onload = () => {
  //     Vue.prototype.$socket = window.io(DOMAIN, {
  //       autoConnect: false
  //     });
  //   };
  // } else {
  const io = require('socket.io-client');
  Vue.prototype.$socket = io({
    autoConnect: false
  });
  // }
};
