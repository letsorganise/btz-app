import { trackEvent } from '../utils';

export default ({ app, router, Vue }) => {
  router.afterEach((to, from, next) => {
    window._paq.push(['setReferrerUrl', from.path]);
    window._paq.push(['setCustomUrl', to.path]);
    window._paq.push(['setDocumentTitle', to.name]);
    window._paq.push(['enableLinkTracking']);
    window._paq.push(['trackPageView']);
    // console.error('page tracked');
  });

  Vue.prototype.$trackEvent = trackEvent;
};
