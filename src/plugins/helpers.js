import { date } from 'quasar';
import timeago from 'timeago.js';
import { notifyErrors, http } from '../utils';

const messageRecieved = new Audio('statics/162464_311243-lq.mp3');

export const helpers = {
  notifyErrors,
  timeago,
  prune(obj) {
    const myObj = Object.assign({}, obj);
    for (const propName in myObj) {
      if (myObj[propName] === null || myObj[propName] === undefined || myObj[propName] === '') {
        delete myObj[propName];
      }
    }
    return myObj;
  },
  filterByAge(data, from, to, dateProp = 'birthDate') {
    const current = new Date();
    if (to >= 1) {
      return data.filter(item => {
        if (item[dateProp]) {
          const age = date.getDateDiff(current, item[dateProp], 'years');
          return age >= from && age <= to;
        }
      }).length;
    }
    // return data count that doesn't have dateProp
    return data.filter(item => !item[dateProp]).length;
  },
  isUUID(str) {
    if (!str || typeof str !== 'string') {
      return false;
    }
    return str.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
  },
  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  playSound() {
    messageRecieved.play();
  },
  truncate(value, length) {
    const len = length || 15;
    if (!value || typeof value !== 'string') return '';
    if (value.length <= len) return value;
    return value.substring(0, len) + '...';
  },
  formatDateTime(dateGiven, fmtStr) {
    return dateGiven ? date.formatDate(dateGiven, fmtStr || 'ddd, DD/MM/YY, hh:mm A') : '';
  },
  formatDate(dateGiven, fmtStr) {
    return dateGiven ? date.formatDate(dateGiven, fmtStr || 'ddd, DD/MM/YY') : '';
  },

  canRead(val) {
    return val >= 4;
  },
  canUpdate(val) {
    return val >= 5;
  },
  canCreate(val) {
    return val >= 6;
  },
  canDelete(val) {
    return val >= 7;
  }
};

export default ({ app, router, Vue }) => {
  Vue.prototype.$h = helpers;
  Vue.prototype.$http = http;

  Vue.filter('truncate', helpers.truncate);

  Vue.filter('formatDate', helpers.formatDate);

  Vue.filter('formatDateTime', helpers.formatDateTime);

  Vue.filter('timeago', date => (date ? timeago().format(date) : 'never'));

  Vue.filter('formatPermission', num => {
    switch (num) {
      case 4:
        return 'Read';
      case 5:
        return 'Read/Update';
      case 6:
        return 'Read/Update/Create';
      case 7:
        return 'Read/Update/Create/Delete';
    }
  });
};
