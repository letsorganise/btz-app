import { date, Notify } from 'quasar';
import axios from 'axios';

export const isDataOld = (timestamp, diffMinutes = 20) => {
  if (!timestamp) {
    return true;
  }
  return date.getDateDiff(new Date(), timestamp, 'minutes') > diffMinutes;
};

export const notifyErrors = ({ response }) => {
  const { data } = response;
  if (!data) return;
  const errors = data.errors;
  // console.error(errors);
  if (Array.isArray(errors)) {
    errors.map(err => {
      Notify.create({
        message: err.msg,
        position: 'top',
        type: 'negative'
      });
    });
  }
  const { headers } = response;
  if (!headers) return;
  const limit = Number(headers['x-ratelimit-limit']);
  const remaining = Number(headers['x-ratelimit-remaining']);
  if (limit) {
    if (remaining) {
      Notify.create({
        message: `Total remaining Attempts ${remaining}`,
        position: 'top',
        type: 'warning'
      });
    } else {
      Notify.create({
        message: `Please wait for 3 minutes before try again!`,
        position: 'top',
        type: 'negative'
      });
    }
  }
};
const baseURL = '';
// if (Platform.is.electron || Platform.is.cordova) {
//   baseURL = 'https://letsorganise.app';
// }

export const apiUrl = baseURL + '/api/v1';
export const uploadsUrl = baseURL + '/api/v1/uploads';

export const http = axios.create({
  baseURL: apiUrl,
  timeout: 15000, // 15 seconds
  withCredentials: true
});

export function trackEvent(...events) {
  // const id = this.$store.getters.me.id;
  window._paq.push(['trackEvent', ...events]);
}
