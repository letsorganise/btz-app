// Configuration for your app

module.exports = function(ctx) {
  // console.error(ctx);
  return {
    plugins: ['helpers', 'socketIo', 'vuelidate', 'analytics', 'fontawesome-pro'],
    css: ['app.styl'],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null
      // 'material-icons'
      // 'mdi'
      // 'fontawesome'
    ],
    supportIE: true,
    build: {
      env: {
        DOMAIN: JSON.stringify('https://hub.letsorganise.uk')
      },
      scopeHoisting: true,
      vueRouterMode: 'hash',
      sourceMap: !process.env.LIVE,
      analyze: process.env.ANALYZE,
      webpackManifest: true,
      extractCSS: true,
      useNotifier: true,
      minify: true,
      extendWebpack(cfg) {
        // console.error(JSON.stringify(cfg.optimization, null, 2));
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/
        });
      }
    },
    devServer: {
      https: false,
      // port: 8080,
      open: false, // opens browser window automatically
      proxy: {
        // proxy all requests starting with /api to jsonplaceholder
        '/api': {
          target: 'http://localhost:3000',
          changeOrigin: true,
          pathRewrite: {
            '^/api': ''
          }
        },
        '/socket.io': {
          target: 'http://localhost:3000',
          changeOrigin: true,
          ws: true
        }
      }
    },

    framework: {
      iconSet: 'fontawesome-pro',
      components: [
        'QAjaxBar',
        'QAlert',
        'QAutocomplete',
        'QBtn',
        'QBtnDropdown',
        'QCard',
        'QCardActions',
        'QCardMain',
        'QCardSeparator',
        'QCardTitle',
        'QChatMessage',
        'QCheckbox',
        'QChip',
        'QCollapsible',
        'QContextMenu',
        'QDatetime',
        'QEditor',
        'QField',
        'QIcon',
        'QInput',
        'QItem',
        'QItemMain',
        'QItemSide',
        'QItemTile',
        'QLayout',
        'QLayoutDrawer',
        'QLayoutFooter',
        'QLayoutHeader',
        'QList',
        'QListHeader',
        'QModal',
        'QModalLayout',
        'QPage',
        'QPageContainer',
        'QPageSticky',
        'QProgress',
        'QResizeObservable',
        'QRouteTab',
        'QScrollArea',
        'QScrollObservable',
        'QSearch',
        'QSelect',
        'QStep',
        'QStepper',
        'QStepperNavigation',
        'QTab',
        'QTable',
        'QTabPane',
        'QTabs',
        'QTd',
        'QToolbar',
        'QToolbarTitle',
        'QTooltip',
        'QTr',
        'QUploader'
      ],
      directives: ['Ripple', 'CloseOverlay'],
      plugins: ['Notify', 'Dialog', 'AppVisibility', 'LocalStorage', 'AppFullscreen']
    },
    animations: ['fadeIn', 'fadeOut'],
    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {},
      manifest: {
        name: "Let's Organise",
        short_name: 'Lets Organise',
        description: "Union Member's hub",
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#00274c',
        icons: [
          {
            src: 'statics/icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      // bundler: 'builder', // or 'packager'
      extendWebpack(cfg) {
        // do something with Electron process Webpack cfg
      },
      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Window only
        // win32metadata: { ... }
      },
      builder: {
        // https://www.electron.build/configuration/configuration
        // appId: 'quasar-app'
      }
    }
  };
};
